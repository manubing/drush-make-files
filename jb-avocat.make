; Includes
; include other make files from local or remote destinations
includes[default-build] = "https://bitbucket.org/manubing/drush-make-files/raw/master/default-build.make"

; specific modules
projects[headerimage][subdir] = contrib
projects[imce][subdir]        = contrib
projects[ping][subdir]        = contrib
projects[gmap][subdir]        = contrib