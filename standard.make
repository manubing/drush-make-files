; Use Drupal 7
core = "7.x"
api = "2"

; include other make files from local or remote destinations
includes[] = "https://bitbucket.org/manubing/drush-make-files/raw/master/default-build.make"

; default subdir for contrib projects
defaults[projects][subdir] = contrib

; Modules make file
projects[devel][subdir]        = contrib
projects[diff][subdir]         = contrib
projects[variable][subdir]     = contrib

projects[search_krumo][subdir] = contrib
;projects[search_krumo][patch][1789134] = "https://drupal.org/files/search_krumo-wrong-commenting-1789134-1.patch"

