; Includes
; include other make files from local or remote destinations
includes[default-build] = "https://bitbucket.org/manubing/drush-make-files/raw/master/default-build.make"

; Modules make file

projects[devel][subdir] = contrib
projects[diff][subdir] = contrib
projects[environment_indicator][subdir] = contrib